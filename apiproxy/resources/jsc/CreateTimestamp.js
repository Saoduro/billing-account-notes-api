var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);
print(timeStamp)

try {
    var requestBody = JSON.parse(context.getVariable("request.content"));
    context.setVariable("request_correlationId", requestBody.correlationId);
} catch (e){
    var messageid = context.getVariable("messageid");
    context.setVariable("request_correlationId", messageid);
}